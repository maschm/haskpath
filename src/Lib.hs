{-|
Module:       Lib
Description:  TODO
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module Lib
    ( someFunc
    ) where

-- | TODO
someFunc :: IO ()
someFunc = putStrLn "someFunc"
