{-|
Module:       Pathfinding
Description:  Implements pathfinding on the graphs implemented in module 'Graph'
              and defines a variety of helper functions used for this task.
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module Pathfinding where

-- friends
import Graph
import Path

-- std
import Data.Foldable (toList)
import Data.List
import Data.Maybe
import qualified Data.Sequence as Sq

-- | Positive infinity.
inf :: Double
inf = 1 / 0

-- | Invokes the pathfinding entry function and passes start and target node
-- to it in form of external node ids.
getPathsByExternalId ::
     String -> Graph -> ExternalNodeId -> ExternalNodeId -> [Path]
getPathsByExternalId mode g extS extT =
  getPaths mode g (getInternalId g extS) (getInternalId g extT)

-- | Invokes the pathfinding entry function and passes start and target node
-- to it in form of 2D coordinates.
getPathsByCoords ::
     String -> Graph -> Double -> Double -> Double -> Double -> [Path]
getPathsByCoords mode g xFrom yFrom xTo yTo =
  getPaths
    mode
    g
    (getInternalIdFromCoords g xFrom yFrom)
    (getInternalIdFromCoords g xTo yTo)

-- | Invokes different pathfinding routines depending on the String flag
-- that was passed to this function.
getPaths :: String -> Graph -> NodeId -> NodeId -> [Path]
getPaths "simple_astar" = aStarToList
getPaths "node_disjunct_paths" = getNodeDisjunctPaths
getPaths "edge_disjunct_paths" = getEdgeDisjunctPaths
getPaths _ = error "undefined pathfinding mode!"

-- | Recursively invokes the A* algorithm in order to find all
-- node disjunct paths between given start and target node.
getNodeDisjunctPaths :: Graph -> NodeId -> NodeId -> [Path]
getNodeDisjunctPaths g = gndp g []

-- | Accumulator function for getNodeDisjunctPaths.
gndp :: Graph -> [Path] -> NodeId -> NodeId -> [Path]
gndp g paths s t =
  case aStar g s t of
    Nothing -> reverse paths -- no more paths found
    Just nextPath ->
      gndp (removeNodeDisjunctPath g nextPath) (nextPath : paths) s t

-- | Recursively invokes the A* algorithm in order to find all
-- edge disjunct paths between given start and target node.
getEdgeDisjunctPaths :: Graph -> NodeId -> NodeId -> [Path]
getEdgeDisjunctPaths g = gedp g []

-- | Accumulator function for getEdgeDisjunctPaths.
gedp :: Graph -> [Path] -> NodeId -> NodeId -> [Path]
gedp g paths s t =
  case aStar g s t of
    Nothing -> reverse paths -- no more paths found
    Just nextPath ->
      gedp (removeEdgeDisjunctPath g nextPath) (nextPath : paths) s t

-- | Returns a list which contains the shortest path if the invoked A*
-- algorithm found one.
aStarToList :: Graph -> NodeId -> NodeId -> [Path]
aStarToList g s t =
  case aStar g s t of
    Nothing -> []
    Just p -> [p]

-- | Does one full iteration of the A* algorithm, including initialization.
aStar :: Graph -> NodeId -> NodeId -> Maybe Path
aStar g s t
  | s == t = error "source == target node!"
  | s < 0 || s >= nc = error "invalid or unknown start node!"
  | t < 0 || t >= nc = error "invalid or unknown target node!"
  | otherwise = expand g [0 .. nc - 1] s t (Just s) [s] [] iPa iDi heuristic
  where
    nc = length $ nodes g
    iPa :: Sq.Seq NodeId -- ^ Initial parent array
    iPa = Sq.update s s $ Sq.fromList $ replicate nc (-1)
    iDi :: Sq.Seq Double -- ^ Initial distance array
    iDi = Sq.update s 0 $ Sq.fromList $ replicate nc inf
    heuristic :: Sq.Seq Double -- ^ Heuristic
    heuristic = getHeuristic "euclidean" g $ fromJust $ getNode g t

-- | Recursively does A*-search expansion steps until the open set is empty,
-- then returns the shortest path (if found) or Nothing if none found.
expand ::
     Graph -- ^ Graph g to perform A* on.
  -> [NodeId] -- ^ A list containing all node ids.
  -> NodeId -- ^ ID of the start node.
  -> NodeId -- ^ ID of the target node.
  -> Maybe NodeId -- ^ The new node selected for expansion, can be Nothing.
  -> [NodeId] -- ^ The list of node ids currently open for expansion.
  -> [NodeId] -- ^ The list of already explored nodes.
  -> Sq.Seq NodeId -- ^ Holds the parent node id of each node.
  -> Sq.Seq Double -- ^ Holds the current distance from start node foreach node.
  -> Sq.Seq Double -- ^ Holds the heuristic score for each node.
  -> Maybe Path -- ^ The pathfinding result, which can be a Path or Nothing.
expand g nodeIds s t curNode openSet closedSet parents dists heuristic =
  case curNode of
    Nothing -> -- no node left worth exploring -> stop expansion.
      if isInfinite (fromJust $ dists Sq.!? t)
        then Nothing
        else pathBT g parents t []
    Just exploredNode -> -- do an expansion step on this mode.
      expand g nodeIds s t newNode newOSet newCSet newParents newDists heuristic
        -- PART 1: expansion on the currently selected node
      where distExpl =
              fromMaybe
                (error "expand: invalid exploredNode!")
                (dists Sq.!? exploredNode)
            discoveredCons = fromMaybe [] (getConnections g exploredNode)
            newCSet = nub $ exploredNode : closedSet -- TODO nub is in O(n²)!
            newOSet =
              nub $
              filter (`notElem` newCSet) (openSet ++ map fst discoveredCons)
            tempDists =
              distsOverCurNode
                (Sq.fromList $ replicate (length nodeIds) inf)
                discoveredCons
                distExpl
            newDists -- Update dist if path via exploredNode is shorter
             =
              if null discoveredCons
                then dists
                else Sq.zipWith min dists tempDists
            newParents -- Parent set to explNode if tempDists[x] < dist[x].
             =
              if null discoveredCons
                then parents
                else Sq.fromList $
                     map
                       (\x ->
                          updateParent
                            (tempDists Sq.!? fst x)
                            (dists Sq.!? fst x)
                            exploredNode
                            (snd x)) $
                     zip nodeIds (toList parents)
        -- PART 2: preparation for the next expansion step
            gxPlusHx = Sq.zipWith (+) newDists heuristic
            newNode =
              getExploredNode
                gxPlusHx
                newOSet
                False
                (fromJust $ newDists Sq.!? t)
                0
                0

-- | Given a series of nodes, constructs a path using given graph g.
pathAssembly :: Graph -> Path -> [NodeId] -> Maybe Path
pathAssembly g pathEdges []
  | null pathEdges = Nothing
  | otherwise = Just pathEdges
pathAssembly g pathEdges [x] = pathAssembly g pathEdges []
pathAssembly g pathEdges (x:y:xs) =
  case getEdge g x y of
    Nothing -> error "pathAssembly: couldn't fetch upcoming edge!"
    Just newEdge -> pathAssembly g (pathEdges ++ [newEdge]) (y : xs)
    -- TODO optimize list cons, maybe use fold? What about empty paths?

-- | Given a graph and the final parent array, constructs a node sequence
-- describing along which the shortest path was found.   
pathBT :: Graph -> Sq.Seq NodeId -> NodeId -> [NodeId] -> Maybe Path
pathBT g parents curNode pathSuccessors =
  case parents Sq.!? curNode of
    Nothing -> error "pathBT: curNode has no parent!"
    Just parentNode ->
      if parentNode == curNode
        then pathAssembly g [] (curNode : pathSuccessors)
        else pathBT g parents parentNode (curNode : pathSuccessors)

-- | Returns the 1st node within the current open set with minimal g(x) + h(x).
-- also has to be smaller than the initial value of curMin, which is dist[t].
getExploredNode ::
     Sq.Seq Double -> [NodeId] -> Bool -> Double -> Int -> Int -> Maybe NodeId
getExploredNode gxPlusHx openSet foundNode curMin curMinId curId =
  case Sq.viewl gxPlusHx of
    Sq.EmptyL ->
      if foundNode
        then Just curMinId
        else Nothing
    (x Sq.:< xs) ->
      if x < curMin && elem curId openSet
        then getExploredNode xs openSet True x curId (curId + 1)
        else getExploredNode xs openSet foundNode curMin curMinId (curId + 1)

-- | Recursively fills in dists to nodes x given in connections with
-- dists[exploredNode] + weight(exploredNode -> x) to form tempDist.
distsOverCurNode :: Sq.Seq Double -> [Connection] -> Double -> Sq.Seq Double
distsOverCurNode tempDists [] _ = tempDists
distsOverCurNode tempDists (c:cs) distToCurNode =
  distsOverCurNode
    (Sq.update (fst c) (distToCurNode + snd c) tempDists)
    cs
    distToCurNode

-- | Yields a new parent for a node x if dist[x] would then decrease.
updateParent :: Maybe Double -> Maybe Double -> NodeId -> NodeId -> NodeId
updateParent tempD curD tempParent curParent
  | isNothing tempD || isNothing curD = error "updateParent: invalid dist!"
  | otherwise =
    if tempD < curD
      then tempParent
      else curParent
