{-|
Module:       Path
Description:  Contains the implementation for paths along edges, including
              utility functions on paths.
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module Path where

-- std
import Data.List (sortBy)

-- friends
import Graph

-- | A path is simply defined as a list of edges.
type Path = [Edge]

-- | Returns the combined weight of given path's edges.
getPathWeight :: Path -> Double
getPathWeight p = sum $ map eWeight p

-- | Removes given path nodes and all incident edges from given graph,
-- excluding the source and target node.
removeNodeDisjunctPath :: Graph -> Path -> Graph
removeNodeDisjunctPath g path = foldl removeNodeById g' nodes
  where nodes = sortBy (flip compare) $ tail $ map fromId path
        g' = removeEdge (removeEdge g (head path)) (last path)

-- | Removes the edges from a given path from the given graph.
removeEdgeDisjunctPath :: Graph -> Path -> Graph
removeEdgeDisjunctPath = foldl removeEdge