{-|
Module:       Util
Description:  TODO
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module Util where

-- | The radius of the earth (in meters).
earthRadius :: Floating a => a
earthRadius = 6378137

-- | TODO trig
rad2deg :: Floating a => a -> a
rad2deg a = a * (180 / pi)

-- | TODO
deg2rad :: Floating a => a -> a
deg2rad a = a / (180 / pi)

-- | TODO
lon2x :: Floating a => a -> a
lon2x lon = deg2rad lon * earthRadius

-- | TODO
x2lon :: Floating a => a -> a
x2lon x = rad2deg (x / earthRadius)

-- | TODO
lat2y :: Floating a => a -> a
lat2y lat = log (tan ((deg2rad lat / 2) + (pi / 4))) * earthRadius

-- | TODO
y2lat :: Floating a => a -> a
y2lat y = rad2deg (2 * (atan (exp (y / earthRadius))) - (pi / 2))
