{-# LANGUAGE DuplicateRecordFields #-}
{-|
Module:       ParseOSM
Description:  TODO
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module ParseOSM where

import Data.HashMap.Strict (HashMap, fromList, (!))
import Text.XML.HXT.Core

import qualified Graph as G
import Util

-- | TODO
parseOsm :: FilePath -> IO G.Graph
parseOsm = fmap (head) . runX . parseXML

-- | TODO
parseXML :: FilePath -> IOSArrow b G.Graph
parseXML src
  = configSysVars [withValidate no, withTrace 0]
    >>>
    readDocument [] src
    >>>
    traceMsg 1 "start reading document"
    >>>
    getChildren >>> isElem >>> xunpickleVal xpOsm
    >>>
    traceValue 1 (show)
    >>>
    arr osmGraphToGraph

-- | TODO
data OsmNode = OsmNode {nodeId :: Int, lat :: Double, lon :: Double} deriving Show

-- | TODO
type OsmWay = [Int]

-- | TODO
data OsmGraph = OsmGraph {nodes :: [OsmNode], ways :: [OsmWay]} deriving Show

-- | TODO
instance XmlPickler OsmNode where
  xpickle = xpElem "node" $
            xpFilterAttr (hasName "id" <+> hasName "lat" <+> hasName "lon") $
            xpFilterCont (hasName "") $
            xpWrap (\(id, lat, lon) -> OsmNode id lat lon,
                    \node -> (nodeId node, lat node, lon node)) $
            xpTriple (xpAttr "id" xpPrim) (xpAttr "lat" xpPrim) (xpAttr "lon" xpPrim)

-- | TODO
xpOsmWay :: PU OsmWay
xpOsmWay = xpElem "way" $
           xpFilterAttr (hasName "") $
           xpFilterCont (hasName "nd") $
           -- xpWrap (\(id, nodes) -> OsmWay id nodes,
           --         \way -> (wayId way, wayNodeIds way)) $
           -- xpPair (xpAttr "id" xpPrim)
           (xpList $ xpElem "nd" $ xpAttr "ref" xpPrim)

-- | TODO
instance XmlPickler OsmGraph where
  xpickle = xpOsm

-- | TODO
xpOsm :: PU OsmGraph
xpOsm = xpElem "osm" $
        xpFilterAttr (hasName "") $
        xpFilterCont (hasName "node" <+> hasName "way") $
        xpWrap (\(nodes, ways) -> OsmGraph nodes ways,
                \graph -> (nodes graph, ways graph)) $
        xpPair (xpList xpickle) (xpList xpOsmWay)

-- | TODO
osmGraphToGraph :: OsmGraph -> G.Graph
osmGraphToGraph (OsmGraph nodes ways) = G.setEuclideanEdgeWeights $ G.graphFromList (map osmNodeToNode nodes) (concatMap wayToEdges ways)
  where newId :: Int -> Int
        newId = (!) $ fromList $ zip (map (nodeId) nodes) [0..]

        osmNodeToNode :: OsmNode -> G.Node
        osmNodeToNode (OsmNode id lat lon) = G.Node id (lon2x lon) (lat2y lat) 1
        -- rounding lat and lon at position 5 keeps a precision of 1.1 meters

        wayToEdges :: OsmWay -> [G.Edge]
        wayToEdges [] = []
        wayToEdges xs = map (\(from,to) -> G.Edge from to 1) $ zip newIds (tail newIds)
          where newIds = map newId xs
