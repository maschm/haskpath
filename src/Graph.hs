{-|
Module:       Graph
Description:  An implementation of a directed graph consisting of weighted
              nodes and edges. A variety of getters/setters and graph
              manipulation functions are provided.
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module Graph where

import Data.Array
import Data.Foldable (toList)
import Data.List (find)

-- import Control.Monad ((<<=))
-- std
import Data.Maybe (fromJust, fromMaybe)
import qualified Data.Sequence as Sq

-- | An alias for the int-typed graph-internal ids of nodes.
type NodeId = Int

-- | An alias for int-typed node ids provided by external sources.
type ExternalNodeId = Int

-- | A graph node holds its external id, its 2D-position and its weight.
data Node = Node
  { extId :: ExternalNodeId
  , x :: Double
  , y :: Double
  , nWeight :: Double
  } deriving (Show, Eq)

-- | Returns the position of a node as a tuple.
pos :: Node -> (Double, Double)
pos (Node _ x y _) = (x, y)

-- | Returns the euclidean distance between the two given nodes.
getEuclideanDist :: Node -> Node -> Double
getEuclideanDist from to = sqrt ((x from - x to) ^^ 2 + (y from - y to) ^^ 2)

-- | A weighted edge connects two nodes given by internal ids fromId and toId.
data Edge = Edge
  { fromId :: NodeId
  , toId :: NodeId
  , eWeight :: Double
  } deriving (Show, Eq)

-- | True if given edge c is incident to the node with id x, False otherwise.
isIncident :: Edge -> NodeId -> Bool
isIncident c x = fromId c == x || toId c == x

-- | True if given edges c1 and c2 run between the same nodes, else False.
isEqual :: Edge -> Edge -> Bool
isEqual c1 c2 =
  fromId c1 == fromId c2 && toId c1 == toId c2 ||
  fromId c1 == toId c2 && toId c1 == fromId c2

-- | True if both start and end nodes are the same for both edges, else False.
isDirEqual :: Edge -> Edge -> Bool
isDirEqual c1 c2 = fromId c1 == fromId c2 && toId c1 == toId c2

-- | Defines the connection to a node given by its id and the edge weight.
type Connection = (NodeId, Double)

-- | A directed graph is defined by a collection of nodes called "nodes" and
-- one collection of connections for each of these nodes, held in "connections".
data Graph = Graph
  { nodes :: Sq.Seq Node
  , connections :: Sq.Seq [Connection]
  } deriving (Show, Eq)

-- | Takes a node list and an edge list and returns the graph described by them.
graphFromList :: [Node] -> [Edge] -> Graph
graphFromList nodes edges =
  Graph
    (Sq.fromList nodes)
    (Sq.fromArray $ accumArray (++) [] bnds (map edgeToIndexedCon edges))
  where
    bnds = (0, length nodes - 1)
    edgeToIndexedCon :: Edge -> (NodeId, [Connection])
    edgeToIndexedCon (Edge fromId toId eWeight) = (fromId, [(toId, eWeight)])

-- | Extracts the id from given graph g corresponding to given coordinates.
-- Returns -1 if no node is found.
getInternalIdFromCoords :: Graph -> Double -> Double -> NodeId
getInternalIdFromCoords g xPos yPos =
  fromMaybe (-1) $
  Sq.findIndexL (\(Node _ x y _) -> (x, y) == (xPos, yPos)) $ nodes g

-- | Given the external id of a node, returns its id inside given graph g.
-- Returns -1 if no node is found.
getInternalId :: Graph -> ExternalNodeId -> NodeId
getInternalId g ext =
  fromMaybe (-1) $ Sq.findIndexL (\(Node extId _ _ _) -> extId == ext) $ nodes g

-- | Extracts the node with given graph-internal id from given graph g.
getNode :: Graph -> NodeId -> Maybe Node
getNode g id = nodes g Sq.!? id

-- | Returns a list of connections going out from node described by given id.
getConnections :: Graph -> NodeId -> Maybe [Connection]
getConnections g id = connections g Sq.!? id

-- | Returns the connection running between the two nodes whose ids were given.
getConnection :: Graph -> NodeId -> NodeId -> Maybe Connection
getConnection g from to = find ((to ==) . fst) =<< getConnections g from

-- | Given a node id, Returns all nodes adjacent to the corresponding node in g.
getNeighbours :: Graph -> NodeId -> Maybe [NodeId]
getNeighbours g id = map fst <$> getConnections g id

-- | Given two node ids, returns the weight of the connection between them.
getWeight :: Graph -> NodeId -> NodeId -> Maybe Double
getWeight g from to = snd <$> getConnection g from to

-- | Given two node ids, returns the weighted edge connecting them.
getEdge :: Graph -> NodeId -> NodeId -> Maybe Edge
getEdge g from to = Edge from to <$> getWeight g from to

-- | Returns a list of all edges present in given graph g.
getEdgeList :: Graph -> [Edge]
getEdgeList (Graph _ edges) = concat $ Sq.mapWithIndex edgeList edges
  where
    edgeList :: Int -> [Connection] -> [Edge]
    edgeList from = map $ uncurry $ Edge from

-- | Adds given node n to given graph g.
addNode :: Graph -> Node -> (Graph, NodeId)
addNode g n = (Graph (ns Sq.|> n) (connections g), length ns)
  where
    ns = nodes g

-- | Adds given edge e to given graph g. 
addEdge :: Graph -> Edge -> Graph
addEdge g e =
  Graph
    (nodes g)
    (Sq.update (fromId e) ((toId e, eWeight e) : consFrom) (connections g))
  where
    consFrom :: [Connection]
    consFrom = connections g `Sq.index` fromId e

-- | In given graph g, sets the weight of the node with given id to w. 
setNodeWeight :: Graph -> NodeId -> Double -> Graph
setNodeWeight g id w =
  case getNode g id of
    Nothing -> g
    Just n ->
      Graph
        (Sq.update id (Node (extId n) (x n) (y n) w) (nodes g))
        (connections g)

-- | updates an existing edge in given graph g.
setEdge :: Graph -> Edge -> Graph
setEdge g (Edge from to weight) =
  Graph (nodes g) (Sq.update from newCons (connections g))
  where
    newCons =
      map
        (\(cTo, cW) ->
           if to == cTo
             then (cTo, weight)
             else (cTo, cW))
        (connections g `Sq.index` from)

-- | In given graph g, updates the weight of the edge between ids from and to.
setEdgeWeight :: Graph -> NodeId -> NodeId -> Double -> Graph
setEdgeWeight g from to w = setEdge g (Edge from to w)

-- | In given graph g, removes the node with given id if found. 
removeNodeById :: Graph -> NodeId -> Graph
removeNodeById g id =
  case getNode g id of
    Nothing -> g
    Just n -> Graph (nodes g) (Sq.fromList updatedCons)
  where
    updatedCons =
      map (filter (\(toId, w) -> toId /= id)) $
      toList $ Sq.deleteAt id $ connections g

-- | In given graph g, removes the directed edge with given endpoints if found.
removeEdge :: Graph -> Edge -> Graph
removeEdge g (Edge from to _) =
  Graph (nodes g) (Sq.update from newL (connections g))
  where
    newL = filter (\(toId, w) -> toId /= to) $ connections g `Sq.index` from

-- | In given graph g, updates all edge weights of the graph to represent
-- the euclidean distance between its nodes.
setEuclideanEdgeWeights :: Graph -> Graph
setEuclideanEdgeWeights g =
  foldl setEdge g $
  map
    (\(Edge from to w) -> Edge from to (getEuclideanDistById g from to))
    (getEdgeList g)

-- | From given graph g, retrieves the euclidean distance between the nodes
-- given by fromId and toId.
getEuclideanDistById :: Graph -> NodeId -> NodeId -> Double
getEuclideanDistById g fromId toId =
  case getNode g fromId of
    Nothing -> -1
    Just from ->
      case getNode g toId of
        Nothing -> -1
        Just to -> getEuclideanDist from to

-- | Returns a heuristic score for every node in given graph g with respect to
-- a given graph node. The type of heuristic computed is chosen with given flag.
getHeuristic :: String -> Graph -> Node -> Sq.Seq Double
getHeuristic "euclidean" = getEuclideanHeuristic
getHeuristic _ = error "undefined heuristic!"

-- | Returns a heuristic score based on the euclidean distance to given
-- graph node tNode for the nodes of given graph g.
getEuclideanHeuristic :: Graph -> Node -> Sq.Seq Double
getEuclideanHeuristic g tNode =
  Sq.fromList [getEuclideanDist n tNode | n <- toList $ nodes g]
