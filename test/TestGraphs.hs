{-|
Module:       TestGraphs
Description:  Defines graphs to be used for the testing of other modules.
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module TestGraphs where

import Graph

nodeA = Node 1000 0 0 1
nodeB = Node 1001 1 0 1
nodeC = Node 1002 2 0 1
nodeD = Node 1003 0 1 1
nodeE = Node 1004 1 1 1
nodeF = Node 1005 2 1 1
nodeG = Node 1006 0 2 1
nodeH = Node 1007 1 2 1
nodeI = Node 1008 2 2 1
nodeJ = Node 1009 2 3 1

aId = 0
bId = 1
cId = 2
dId = 3
eId = 4
fId = 5
gId = 6
hId = 7
iId = 8
jId = 9

bigGraph = graphFromList
    [nodeA, nodeB, nodeC, nodeD, nodeE, nodeF, nodeG, nodeH, nodeI, nodeJ]
    [Edge aId iId 10, Edge aId dId 2, Edge aId eId 1,
    Edge bId aId  1, Edge bId cId 1, Edge bId gId 1,
    Edge cId fId  1,
    Edge dId hId  6, Edge dId eId 1,
    Edge eId bId  1, Edge eId dId 1, Edge eId fId 6, Edge eId iId 8, Edge eId hId 4,
    Edge fId bId  1, Edge fId iId 1,
    Edge gId hId  2,
    Edge hId iId  2,
    Edge jId iId  1]

-- smallGraph
--
--   A---C
--    \ /
--     E
--      \
--       I

smallGraph = graphFromList
    [nodeA, nodeC, nodeE, nodeI]
    [Edge 0 2 1, Edge 0 1 1,
    Edge 2 0 1, Edge 2 3 1, Edge 2 1 1,
    Edge 3 2 1,
    Edge 1 0 1, Edge 1 2 1]
