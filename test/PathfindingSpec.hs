{-|
Module:       PathfindingSpec
Description:  Test cases for the module 'Pathfinding'.
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module PathfindingSpec where

import Test.Hspec
import Control.Exception (evaluate)

import Graph
import Path
import TestGraphs
import Pathfinding

-- | Test cases for the module 'Pathfinding'.
spec :: Spec
spec = do
  describe "aStar" $ do
    it "start == end => throw error" $
      evaluate (aStar bigGraph (-1) (-1)) `shouldThrow` errorCall "source == target node!"

    describe "on smallGraph" $ do
      it "one length path" $
        aStar smallGraph 0 1 `shouldBe` Just [Edge 0 1 1]

      it "two length path" $
        aStar smallGraph 0 3 `shouldBe` Just [Edge 0 2 1, Edge 2 3 1]

    describe "on bigGraph" $ do
      it "path 0 to 8" $
        aStar bigGraph 0 8 `shouldBe` Just [Edge 0 4 1, Edge 4 1 1, Edge 1 2 1, Edge 2 5 1, Edge 5 8 1]

      it "path 0 to 9" $
        aStar bigGraph 0 9 `shouldBe` Nothing

      it "path 9 to 8" $
        aStar bigGraph 9 8 `shouldBe` Just [Edge 9 8 1]

  describe "getNodeDisjunctPaths" $ do
    it "paths 0 to 8" $
      getNodeDisjunctPaths bigGraph 0 8 `shouldBe` [[Edge 0 4 1, Edge 4 1 1, Edge 1 2 1, Edge 2 5 1, Edge 5 8 1], [Edge 0 3 2, Edge 3 7 6, Edge 7 8 2], [Edge 0 8 10]]

    it "paths 0 to 9" $
      getNodeDisjunctPaths bigGraph 0 9 `shouldBe` []

    it "paths 9 to 8" $
      getNodeDisjunctPaths bigGraph 9 8 `shouldBe` [[Edge 9 8 1]]


  describe "getEdgeDisjunctPaths" $ do
    it "paths 0 to 8" $
      getEdgeDisjunctPaths bigGraph 0 8 `shouldBe` [[Edge 0 4 1, Edge 4 1 1, Edge 1 2 1, Edge 2 5 1, Edge 5 8 1], [Edge 0 3 2, Edge 3 4 1, Edge 4 7 4, Edge 7 8 2], [Edge 0 8 10]]

    it "paths 0 to 9" $
      getEdgeDisjunctPaths bigGraph 0 9 `shouldBe` []

    it "paths 9 to 8" $
      getEdgeDisjunctPaths bigGraph 9 8 `shouldBe` [[Edge 9 8 1]]

  describe "getPaths" $ do
    it "mode: simple_astar" $
      getPaths "simple_astar" bigGraph 0 8 `shouldBe` aStarToList bigGraph 0 8

    it "invalid mode" $
      evaluate (getPaths "s") `shouldThrow` errorCall "undefined pathfinding mode!"