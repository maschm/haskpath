{-|
Module:       PathSpec
Description:  Test cases for the module 'Path'.
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module PathSpec where

import Test.Hspec

import Graph
import Path

-- | Test cases for the module 'Path'.
spec :: Spec
spec = do
    describe "getPathWeight" $ do
        it "non-empty path" $
            getPathWeight [Edge 0 1 13, Edge 1 2 (-9), Edge 2 0 7] `shouldBe` 11

        it "empty path" $
            getPathWeight [] `shouldBe` 0