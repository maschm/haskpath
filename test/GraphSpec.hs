{-|
Module:       GraphSpec
Description:  Test cases for module 'Graph'.
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module GraphSpec where

import Test.Hspec
import Data.Sequence

import Graph

--- Basic graph for testing
--   A---D
--    \ /
--     B
--      \
--       C

nodeA = Node aId 0 0 1
nodeB = Node bId 1 1 1
nodeC = Node cId 2 2 1
nodeD = Node dId 0 2 1
aId = 0
bId = 1
cId = 2
dId = 3
basicGraph = Graph
             (fromList [nodeA, nodeB, nodeC, nodeD])
             (fromList [[(bId, 1), (dId, 1)],
                               [(aId, 1), (cId, 1), (dId, 1)],
                               [(bId, 1)],
                               [(aId, 1), (bId, 1)]])

basicGraphFromList = graphFromList
                     [nodeA, nodeB, nodeC, nodeD]
                     [Edge aId bId 1, Edge aId dId 1,
                      Edge bId aId 1, Edge bId cId 1, Edge bId dId 1,
                      Edge cId bId 1,
                      Edge dId aId 1, Edge dId bId 1]

-- | Test cases for module 'Graph'.
spec :: Spec
spec = do
  describe "graphFromList" $ do
    it "is equal on basic graph" $
      basicGraphFromList `shouldBe` basicGraph
