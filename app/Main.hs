{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-|
Module:       Main
Description:  TODO
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module Main where

import System.Console.CmdArgs hiding ((:=))
import Data.List
import Data.Maybe
import Diagrams.Size
import Diagrams.Backend.Cairo
import Diagrams.Backend.Gtk
import Graphics.UI.Gtk

import Lib
import Graph
import ParseOSM
import Draw

-- | Hackpath TODO
data Hackpath = Hackpath
  { file :: FilePath
  , out :: FilePath
  , format :: String
  }
  deriving (Show, Data, Typeable)

hackpath = Hackpath
  { file = def &= argPos 0 &= typ "FILE"
  , out = def &= typ "FILE" &= help "the output path, the extension defines the file type ('png', 'ps', 'pdf' and 'svg' are supported)"
  , format = "auto" &= typ "FORMAT" &= help "the format of the input file ('auto', 'basic' or 'osm')"
  }
  -- &= verbosity
  -- &= help "Suggest improvements to Haskell source code"
  -- &= summary "Hackpath v0.0, some path algorithms in haskell"
  -- &= details ["Hlint gives hints on how to improve Haskell code",""
  -- ,"To check all Haskell files in 'src' and generate a report type:","  hlint src --report"]

-- | Format TODO
data Format = Basic | OSM deriving (Show, Data, Typeable)

-- | Config TODO
data Config = Config
  { file :: FilePath
  , output :: Maybe FilePath
  , format :: Format
  }
  deriving (Show, Data, Typeable)

-- | readConfig TODO
readConfig :: Hackpath -> Config
readConfig (Hackpath file out format) = Config file o f
  where f = if isSuffixOf ".osm" file then OSM else Basic
        o = if null out then Nothing else Just out

-- | main TODO
main :: IO ()
main = do
  config <- fmap (readConfig) $ cmdArgs hackpath
  print "Config:"
  print config
  graph <- importGraph config
  if isNothing (output config)
    then showGraph config graph
    else outputGraph config graph
  return ()

-- | importGraph TODO
importGraph :: Config -> IO Graph
importGraph (Config file _ OSM) = parseOsm file
-- importGraph (Config file _ Basic) = parseBasic file

-- | showGraph TODO
showGraph :: Config -> Graph -> IO ()
showGraph _ g = do
  initGUI
  window <- windowNew
  set window [windowTitle := "Haskpath",
              windowDefaultWidth := 600,
              windowDefaultHeight := 600,
              containerBorderWidth := 30 ]
  frame <- frameNew
  containerAdd window frame
  drawingArea <- drawingAreaNew
  containerAdd frame drawingArea
  widgetShowAll window
  drawingArea `onExpose` (\x -> do defaultRender drawingArea (diagram g)
                                   return False)
  window `onDestroy` mainQuit
  mainGUI

-- | outputGraph TODO
outputGraph :: Config -> Graph -> IO ()
outputGraph (Config _ (Just output) _) g = renderCairo output absolute (diagram g)
outputGraph _ _ = error "invalid output path"
