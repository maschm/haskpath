{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-|
Module:       Draw
Description:  TODO
Copyright:    (c) Matthias Schmitt, 2018
                  Andreas Boltres, 2018
License:      BSD3
Maintainer :  male.schmitt@posteo.de
-}
module Draw where

import Diagrams.Prelude
import Diagrams.Backend.Cairo
import Data.List (nubBy)
import Data.Sequence (mapWithIndex)
import Data.Maybe (fromJust)
import Data.Foldable (toList)

import Graph

-- graphBounds :: Graph -> (Float, Float, Float, Float)

-- | diagram TODO
diagram :: Graph -> Diagram B
diagram g = mconcat $ (atPoints points nodeDias) : edgeDias
  where points :: [P2 Double]
        points = toList $ fmap (p2 . pos) (nodes g)
        edges :: [Edge]
        edges = nubBy isEqual $ getEdgeList g
        nodeDias :: [Diagram B]
        nodeDias = toList $ mapWithIndex nodeToDia (nodes g)
        edgeDias :: [Diagram B]
        edgeDias = map (edgeToDia g) edges

-- | nodeToDia TODO
nodeToDia :: Int -> Node -> Diagram B
nodeToDia id (Node _ _ _ w) = text ((show id) ++ "\n" ++ (show w)) # fontSizeL 2 # fc black <> circle 2 # fc green # lw none # named id

-- | edgeToDia TODO
edgeToDia :: Graph -> Edge -> Diagram B
edgeToDia g (Edge fromId toId weight) = arrowBetween' arrowStyle from to # lw ultraThin
  where from :: P2 Double
        from = p2 $ pos $ fromJust $ getNode g fromId
        to :: P2 Double
        to = p2 $ pos $ fromJust $ getNode g toId

-- | arrowStyle TODO
arrowStyle = (with & arrowHead .~ noTail)
